from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView

import session_csrf
session_csrf.monkeypatch()

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^$', TemplateView.as_view(template_name='home.html'), name="home"),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    # blog app urls
    (r'^blog/', include('blog.urls')),

    url(r'^_ah/', include('djangae.urls')),

    url(r'^csp/', include('cspreports.urls')),
)